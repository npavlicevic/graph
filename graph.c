#include "graph.h"
/*
* Function: random_number
* ```````````````````````
* find a random number
*
* seed: starting number
*
* return: next number in sequence
*/
DLONG random_number(DLONG seed) {
  DLONG mod = 0xffffffff;
  DLONG a = 1664525;
  DLONG c = 1013904223;
  DLONG next = a * seed + c;
  return next & mod;
}
Matrix *matrix_create(int rows, int cols) {
  Matrix *this = (Matrix*)calloc(1, sizeof(Matrix));
  this->rows = rows;
  this->cols = cols;
  this->m = (int**)calloc(this->rows, sizeof(int*));
  int i = 0;
  for(;i < this->rows; i++) {
    this->m[i] = (int*)calloc(this->cols, sizeof(int));
  }
  return this;
}
/*
* Function: matrix_russians_boolean
* ````````````````````````````````
* finds all possible products of boolean vector
*
* this: matrix to fill
*
* return: nothing
*/
void matrix_russians_boolean(Matrix *this) {
  int i = 1, j, up, _up;
  for(up = this->rows; i < up; i++) {
    for(j = 1, _up = this->rows; j < _up; j++) {
      this->m[i][j] = scalar_multiply_boolean(i, j);
    }
  }
}
/*
* Function: matrix_multiply_boolean
* `````````````````````````````````
* find product of boolean matrices
* AB=C
* A ~ a x b B ~ b x c C ~ a x c
*
* this: matrix to compute
* a: left operand
* b: right operand
*
* return: nothing
*/
void matrix_multiply_boolean(Matrix *this, Matrix *a, Matrix *b) {
  int i = 0, j, k;
  int a_rows = a->rows;
  int a_cols = a->cols;
  int b_cols = b->cols;
  for(; i < a_rows; i++) {
    for(j = 0; j < b_cols; j++) {
      for(k = 0; k < a_cols; k++) {
        this->m[i][j] |= a->m[i][k] & b->m[k][j];
      }
    }
  }
}
/*
* Function: matrix_multiply_russians_boolean
* ``````````````````````````````````````````
* find product of boolean matrices using four russians method
* AB=C
* A ~ a x b B ~ b x c C ~ a x c
*
* this: matrix to compute
* russians: precomputed boolean vector products
* a: left operand
* b: right operand
*
* return: nothing
*/
void matrix_multiply_russians_boolean(Matrix *this, Matrix *russians, Matrix *a, Matrix *b) {
  int i = 0, j, k;
  int a_rows = a->rows;
  int a_cols = a->cols;
  int step = log(a_cols) / log(2);
  int b_cols = b->cols;
  int row;
  int col;
  for(; i < a_rows; i++) {
    for(j = 0; j < b_cols; j++) {
      for(k = 0; k < a_cols; k += step) {
        row = matrix_row_boolean(a, i, k, step);
        col = matrix_column_boolean(b, k, j, step);
        this->m[i][j] |= russians->m[row][col];
      }
    }
  }
}
/*
* Function: matrix_row_boolean
* ````````````````````````````
* express boolean matrix row as integer
*
* this: matrix to compute
* row
* column
* step: number of boolean values to take
*
* return: nothing
*/
int matrix_row_boolean(Matrix *this, int row, int column, int step) {
  int i = column, up = column + step, r = 0;
  for(; i < up; i++) {
    r <<= 1;
    r |= this->m[row][i];
  }
  return r;
}
/*
* Function: matrix_column_boolean
* ```````````````````````````````
* express boolean matrix column as integer
*
* this: matrix to compute
* row
* column
* step: number of boolean values to take
*
* return: nothing
*/
int matrix_column_boolean(Matrix *this, int row, int column, int step) {
  int i = row, up = row + step, r = 0;
  for(; i < up; i++) {
    r <<= 1;
    r |= this->m[i][column];
  }
  return r;
}
/*
* Function: scalar_multiply_boolean
* `````````````````````````````````
* finds product of boolean vectors
* boolean product ~ or_i (one_i and two_i) for boolean digit i
*
* one: first boolean vector
* two: second boolean vector
*
* return: product of boolean vectors
*/
int scalar_multiply_boolean(int one, int two) {
  int r = 0, digit_one, digit_two;
  for(;one && two;) {
    digit_one = one & 0x01;
    digit_two = two & 0x01;
    r = r | (digit_one & digit_two);
    one >>= 1;
    two >>= 1;
  }
  return r;
}
/*
* Function: matrix_set
* ````````````````````
* sets matrix value
*
* this: matrix
* col: column to set
* row: row to set
* value: value to set
*
* return: nothing
*/
void matrix_set(Matrix *this, int col, int row, int value) {
  this->m[col][row] = value;
}
/*
* Function: matrix_set_from_array
* ```````````````````````````````
* sets matrix value
*
* this: matrix
* array: array containing values
*
* return: nothing
*/
void matrix_set_from_array(Matrix *this, int *array) {
  int i = 0, j, k = 0;
  for(; i < this->rows; i++) {
    for(j = 0; j < this->cols; j++) {
      this->m[i][j] = array[k++];
    }
  }
}
/*
* Function: matrix_copy
* `````````````````````
* copies src matrix to this
*
* this: matrix
* src: matrix
*
* return: nothing
*/
void matrix_copy(Matrix *this, Matrix *src) {
  int i = 0, j;
  for(; i < src->rows; i++) {
    for(j = 0; j < src->cols; j++) {
      matrix_set(this, i, j, src->m[i][j]);
    }
  }
}
/*
* Function: matrix_print
* ``````````````````````
* prints matrix
*
* this: matrix
*
* return: nothing
*/
void matrix_print(Matrix *this) {
  int i = 0, j, up, _up;
  for(up = this->rows; i < up; i++) {
    for(j = 0, _up = this->cols; j < _up; j++) {
      printf("%d ", this->m[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}
void matrix_destroy(Matrix **this) {
  int i = 0;
  for(; i < (*this)->rows; i++) {
    free((*this)->m[i]);
  }
  free((*this)->m);
  free(*this);
  *this = NULL;
}
Q *q_create(int capacity) {
  Q *this = (Q*)calloc(1, sizeof(Q));
  this->capacity = capacity;
  this->first = this->last = 0;
  this->items = (int*)calloc(this->capacity, sizeof(int));
  return this;
}
void q_push(Q *this, int item) {
  this->items[this->last++] = item;
}
int q_fetch(Q *this) {
  return this->items[this->first];
}
void q_pop(Q *this) {
  this->first++;
}
void q_destroy(Q **this) {
  free((*this)->items);
  free(*this);
  *this = NULL;
}
GraphItem *graph_item_create(int key, int from, int to) {
  GraphItem *this = calloc(1, sizeof(GraphItem));
  this->key = key;
  this->from = from;
  this->to = to;
  return this;
}
void graph_item_destroy(GraphItem **this) {
  free(*this);
  *this = NULL;
}
DLONG graph_item_compare(void *other, void *key) {
  GraphItem *_other = (GraphItem*)other;
  GraphItem *_key = (GraphItem*)key;
  if(_other->key < _key->key) {
    return -1;
  } else if(_other->key > _key->key) {
    return 1;
  }
  return 0;
}
GraphList *graph_list_create(int capacity) {
  GraphList *this = (GraphList*)calloc(1, sizeof(GraphList));
  this->length = 0;
  this->capacity = capacity;
  this->items = (int*)calloc(this->capacity, sizeof(int));
  return this;
}
void graph_list_append(GraphList *this, int item) {
  this->items[this->length++] = item;
}
int graph_list_get(GraphList *this) {
  return this->items[this->length - 1];
}
void graph_list_pop(GraphList *this) {
  this->length--;
}
void graph_list_map_set(GraphList *this, int item) {
  this->items[item] = 1;
  this->length++;
}
void graph_list_map_unset(GraphList *this, int item) {
  this->items[item] = 0;
  this->length--;
}
int graph_list_map_is_set(GraphList *this, int item) {
  return this->items[item];
}
void graph_list_destroy(GraphList **this) {
  free((*this)->items);
  (*this)->items = NULL;
  free(*this);
  *this = NULL;
}
Graph *graph_create(int capacity) {
  int i = 0;
  Graph *this = (Graph*)calloc(1, sizeof(Graph));
  this->capacity = capacity;
  this->edges = 0;
  this->items = (GraphList**)calloc(this->capacity, sizeof(GraphList));
  for(; i < this->capacity; i++) {
    this->items[i] = graph_list_create(this->capacity);
  }
  return this;
}
void graph_edge_insert(Graph *this, int from, int to) {
  graph_list_append(this->items[from], to);
  graph_list_append(this->items[to], from);
  this->edges += 2;
}
void graph_edge_directed_insert(Graph *this, int from, int to) {
  graph_list_append(this->items[from], to);
  this->edges += 1;
}
void graph_edge_weighted_insert(Graph *this, int from, int to, int weight) {
  graph_list_append(this->items[from], to);
  graph_list_append(this->items[from], weight);
  graph_list_append(this->items[to], from);
  graph_list_append(this->items[to], weight);
  this->edges += 2;
}
void graph_edge_weighted_directed_insert(Graph *this, int from, int to, int weight) {
  graph_list_append(this->items[from], to);
  graph_list_append(this->items[from], weight);
  this->edges += 1;
}
int graph_is_adjacent(Graph *this, int from, int to) {
  GraphList *graph_list = this->items[from];
  if(graph_list == NULL) {
    return FALSE;
  }
  int i = 0, up = graph_list->length;
  for(; i < up; i++) {
    if(graph_list->items[i] == to) {
      return TRUE;
    }
  }
  return FALSE;
}
/*
* Function: graph_bfs
* ```````````````````
* finds distance of nodes from source and spanning tree of a graph
* it can also be used to check if graph is connected or not
*
* this: the graph to use
* from: source vertex
* path: map containing parent of each vertex
* distance: map containing distance of each node from source vertex
*
* return: nothing
*/
void graph_bfs(Graph *this, int from, int *path, int *distance) {
  Q *q = q_create(this->capacity * this->capacity);
  int i = 0, up;
  int item, child;
  int _distance = 0;
  q_push(q, from);
  path[from] = from;
  distance[from] = _distance;
  for(;q->first != q->last;) {
    item = q_fetch(q);
    q_pop(q);
    for(i = 0, up = this->items[item]->length; i < up; i++) {
      child = this->items[item]->items[i];
      if(distance[child]) continue;
      q_push(q, child);
      path[child] = item;
      distance[child] = distance[item] + 1;
    }
  }
  q_destroy(&q);
}
/*
* Function: graph_recursive_dfs
* `````````````````````````````
* finds distance of nodes from source and spanning tree of a graph
* it can also be used to check if graph is connected or not
*
* this: the graph to use
* from: source vertex
* path: map containing parent of each vertex
* distance: map containing distance of each node from source vertex
*
* return: nothing
*/
void graph_recursive_dfs(Graph *this, int from, int *path, int *distance) {
  int i, up, child;
  for(i = 0, up = this->items[from]->length; i < up; i++) {
    child = this->items[from]->items[i];
    if(distance[child]) continue;
    path[child] = from;
    distance[child] = distance[from] + 1;
    graph_recursive_dfs(this, child, path, distance);
  }
}
/*
* Function: graph_iterative_dfs
* `````````````````````````````
* finds distance of nodes from source and spanning tree of a graph
* it can also be used to check if graph is connected or not
*
* this: the graph to use
* from: source vertex
* path: map containing parent of each vertex
* distance: map containing distance of each node from source vertex
*
* return: nothing
*/
void graph_iterative_dfs(Graph *this, int from, int *path, int *distance) {
  int i, up, item, child;
  GraphList *stack = graph_list_create(this->capacity * this->capacity);
  graph_list_append(stack, from);
  while(stack->length) {
    item = graph_list_get(stack);
    graph_list_pop(stack);
    for(i = 0, up = this->items[item]->length; i < up; i++) {
      child = this->items[item]->items[i];
      if(distance[child]) continue;
      path[child] = item;
      distance[child] = distance[item] + 1;
      graph_list_append(stack, child);
    }
  }
  graph_list_destroy(&stack);
}
/*
* Function: graph_naive_dijkstra
* ``````````````````````````````
* finds distance of nodes from source and spanning tree of a distance graph
* it can also be used to check if graph is connected or not
* uses q it is same as bfs
*
* this: the graph to use
* from: source vertex
* path: map containing parent of each vertex
* distance: map containing distance of each node from source vertex
*
* return: nothing
*/
void graph_naive_dijkstra(Graph *this, int from, int *path, int *distance) {
  Q *q = q_create(this->capacity * this->capacity);
  int i = 0, up;
  int item, child, weight;
  int _distance = 0;
  q_push(q, from);
  path[from] = from;
  distance[from] = _distance;
  for(;q->first != q->last;) {
    item = q_fetch(q);
    q_pop(q);
    for(i = 0, up = this->items[item]->length; i < up; i += 2) {
      child = this->items[item]->items[i];
      weight = this->items[item]->items[i+1];
      if(distance[child]) continue;
      q_push(q, child);
      path[child] = item;
      distance[child] = distance[item] + weight;
    }
  }
  q_destroy(&q);
}
/*
* Function: graph_map_dijkstra
* ````````````````````````````
* finds distance of nodes from source and spanning tree of a distance graph
* it can also be used to check if graph is connected or not
* uses map to find paths
* n passes, n searches in a pass
* O(n`2) complexity
*
* this: the graph to use
* from: source vertex
* path: map containing parent of each vertex
* distance: map containing distance of each node from source vertex
*
* TODO: in parallel
*
* return: nothing
*/
void graph_map_dijkstra(Graph *this, int from, int *path, int *distance) {
  GraphList *map = graph_list_create(this->capacity);
  int i = 0, up, child, weight;
  int _distance = 0, _distance_min, _distance_min_i;
  for(up = this->capacity; i < up; i++) {
    if(this->items[i] == NULL) continue;
    distance[i] = INT_MAX;
    graph_list_map_set(map, i);
    // TODO use set or map lib here
  }
  distance[from] = _distance;
  while(map->length) {
    _distance_min = INT_MAX;
    _distance_min_i = -1;
    for(i = 0, up = this->capacity; i < up; i++) {
      if(_distance_min > distance[i] && graph_list_map_is_set(map, i)) {
        _distance_min = distance[i];
        _distance_min_i = i;
      }
    }
    if(_distance_min_i < 0) break;
    graph_list_map_unset(map, _distance_min_i);
    for(i = 0, up = this->items[_distance_min_i]->length; i < up; i += 2) {
      child = this->items[_distance_min_i]->items[i];
      weight = this->items[_distance_min_i]->items[i + 1];
      if(distance[child] < INT_MAX) continue;
      path[child] = _distance_min_i;
      distance[child] = distance[_distance_min_i] + weight;
    }
  }
  graph_list_destroy(&map);
}
/*
* Function: graph_heap_dijkstra
* `````````````````````````````
* finds distance of nodes from source and spanning tree of a distance graph
* it can also be used to check if graph is connected or not
* uses heap ~ priority list
* O(n log n) complexity
*
* this: the graph to use
* from: source vertex
* path: map containing parent of each vertex
* distance: map containing distance of each node from source vertex
*
* return: nothing
*/
void graph_heap_dijkstra(Graph *this, int from, int *path, int *distance) {
  HeapArray *heap = heap_array_create(this->capacity+1);
  int i = 0, up, child, weight;
  int _distance = 0;
  GraphItem *item = graph_item_create(_distance, from, 0);
  for(up = this->capacity; i < up; i++) {
    if(this->items[i] == NULL) continue;
    distance[i] = INT_MAX;
  }
  distance[from] = _distance;
  heap_array_push_and_up(heap, item, -1, graph_item_compare);
  for(;heap->position;) {
    item = heap_array_pop_and_down(heap, -1, graph_item_compare);
    if(item == NULL) {
      break;
    }
    for(i = 0, up = this->items[item->from]->length; i < up; i += 2) {
      child = this->items[item->from]->items[i];
      weight = this->items[item->from]->items[i+1];
      if(distance[child] < INT_MAX) continue;
      path[child] = item->from;
      distance[child] = distance[item->from] + weight;
      heap_array_push_and_up(heap, graph_item_create(distance[child], child, 0), -1, graph_item_compare);
    }
    graph_item_destroy(&item);
  }
  heap_array_destroy(&heap);
}
/*
* Function: graph_bellman
* ```````````````````````
* finds shortest paths from source in distance graph
* less efficient than dijkstra but easier to implement
* each iteration go through all edges
* until convergence
* O(mn) complexity
*
* this: the graph to use
* from: source vertex
* path: map containing vertex parent
* distance: map containing vertex distance
*
* return: nothing
*/
void graph_bellman(Graph *this, int from, int *path, int *distance) {
  int i = 0, j, up, _up, updates = 1;
  int child, weight;
  int _distance = 0;
  for(up = this->capacity; i < up; i++) {
    if(this->items[i] == NULL) continue;
    distance[i] = INT_MAX / 2;
  }
  distance[from] = _distance;
  for(;updates;) {
    updates = 0;
    for(i = 0, up = this->capacity; i < up; i++) {
      if(this->items[i] == NULL) continue;
      for(j = 0, _up = this->items[i]->length; j < _up; j += 2) {
        child = this->items[i]->items[j];
        weight = this->items[i]->items[j+1];
        if(distance[i] + weight < distance[child]) {
          distance[child] = distance[i] + weight;
          path[child] = i;
          updates = 1;
        }
      }
    }
  }
}
/*
* Function: graph_floyd
* `````````````````````
* finds shortest path for any two vertices
* based on reccurence D[i][j] = min(D[i][j], D[i][k]+D[k][j])
* O(n`3) complexity
*
* this: graph
* path: matrix containing path
* distance: matrix containing distance
*
* return: nothing
*/
void graph_floyd(Graph *this, Matrix *path, Matrix *distance) {
  int i = 0, j, k, up, _up, __up;
  int child, weight;
  for(up = this->capacity; i < up; i++) {
    for(j = 0, _up = this->capacity; j < _up; j++) {
      if(i == j) {
        distance->m[i][j] = 0;
        continue;
      }
      distance->m[i][j] = INT_MAX / 2;
    }
  }
  for(i = 0, up = this->capacity; i < up; i++) {
    if(this->items[i] == NULL) continue;
    for(j = 0, _up = this->items[i]->length; j < _up; j += 2) {
      child = this->items[i]->items[j];
      weight = this->items[i]->items[j+1];
      path->m[i][child] = child;
      distance->m[i][child] = weight;
    }
  }
  for(k = 0, __up = this->capacity; k < __up; k++) {
    for(i = 0, up = this->capacity; i < up; i++) {
      for(j = 0, _up = this->capacity; j < _up; j++) {
        if(distance->m[i][j] > distance->m[i][k] + distance->m[k][j]) {
          distance->m[i][j] = distance->m[i][k] + distance->m[k][j];
          path->m[i][j] = k;
        }
      }
    }
  }
}
/*
* Function: graph_floyd_path
* ``````````````````````````
* finds path for two vertices
*
* this: map containing path
* from: start vertex
* to: end vertex
* path: matrix containing path
*
* return: nothing
*/
void graph_floyd_path(int *this, int from, int to, Matrix *path) {
  int _from;
  for(;from != to;) {
    _from = from;
    from = path->m[from][to];
    this[from] = _from;
  }
}
/*
* Function: graph_floyd_transitive
* ````````````````````````````````
* checks if there is a path between two vertices ~ aka finds transitive closure
*
* this: map containing path
* transitive: matrix containing path
*
* return: nothing
*/
void graph_floyd_transitive(Graph *this, Matrix *transitive) {
  int i = 0, j, k, up, _up, __up;
  int child;
  for(up = this->capacity; i < up; i++) {
    for(j = 0, _up = this->capacity; j < _up; j++) {
      if(i == j) {
        transitive->m[i][j] = 1;
        continue;
      }
      transitive->m[i][j] = 0;
    }
  }
  for(i = 0, up = this->capacity; i < up; i++) {
    if(this->items[i] == NULL) continue;
    for(j = 0, _up = this->items[i]->length; j < _up; j += 2) {
      child = this->items[i]->items[j];
      transitive->m[i][child] = 1;
    }
  }
  for(k = 0, __up = this->capacity; k < __up; k++) {
    for(i = 0, up = this->capacity; i < up; i++) {
      for(j = 0, _up = this->capacity; j < _up; j++) {
        transitive->m[i][j] = transitive->m[i][j] || (transitive->m[i][k] && transitive->m[k][j]);
      }
    }
  }
}
/*
* Function: graph_adjacency_matrix
* ````````````````````````````````
* make adjacency matrix of unweighted graph
*
* this: graph
* adj: adjacency matrix
*
* return: nothing
*/
void graph_adjacency_matrix(Graph *this, Matrix *adj) {
  int i = 0, j, up, _up, child;
  for(up = this->capacity; i < up; i++) {
    if(this->items[i] == NULL) continue;
    for(j = 0, _up = this->items[i]->length; j < _up; j++) {
      child = this->items[i]->items[j];
      adj->m[i][child] = 1;
    }
  }
}
/*
* Function: graph_adjacency_matrix_weighted
* `````````````````````````````````````````
* make adjacency matrix of weighted graph
*
* this: graph
* adj: adjacency matrix
*
* return: nothing
*/
void graph_adjacency_matrix_weighted(Graph *this, Matrix *adj) {
  int i = 0, j, up, _up, child, weight;
  for(up = this->capacity; i < up; i++) {
    if(this->items[i] == NULL) continue;
    for(j = 0, _up = this->items[i]->length; j < _up; j += 2) {
      child = this->items[i]->items[j];
      weight = this->items[i]->items[j+1];
      adj->m[i][child] = weight;
    }
  }
}
/*
* Function: graph_adjacency_matrix_weighted_boolean
* `````````````````````````````````````````````````
* make boolean adjacency matrix of weighted graph
*
* this: graph
* adj: adjacency matrix
*
* return: nothing
*/
void graph_adjacency_matrix_weighted_boolean(Graph *this, Matrix *adj) {
  int i = 0, j, up, _up, child;
  for(up = this->capacity; i < up; i++) {
    if(this->items[i] == NULL) continue;
    for(j = 0, _up = this->items[i]->length; j < _up; j += 2) {
      child = this->items[i]->items[j];
      adj->m[i][child] = 1;
    }
  }
}
/*
* Function: graph_boolean_transitive
* ``````````````````````````````````
* use boolean adjacency matrix to find transitive closure
*
* this: graph
* russians: matrix containing precomuted boolean vector products
* a: adjacency matrix
* b: adjacency matrix (same as a)
* degree: number of multiplications
*
* return: nothing
*/
void graph_boolean_transitive(Matrix *this, Matrix *russians, Matrix *a, Matrix *b, int degree) {
  int i = 0;
  Matrix *temp;
  for(; i < degree; i++) {
    matrix_multiply_russians_boolean(this, russians, a, b);
    temp = a;
    a = this;
    this = temp;
    // TODO move this to a function
  }
  matrix_copy(this, a);
}
/*
* Function: graph_kruskal_min_spanning_tree
* `````````````````````````````````````````
* find minimum spanning tree using kruskal algorithm
* greedy
* order edges in ascending order
* take edge and add to tree if it does not form cycle (if end vertex has no parent)
* O(log |V|) complexity
*
* this: graph
* path: map to store parents (construct a tree)
*
* return: nothing
*/
void graph_kruskal_min_spanning_tree(Graph *this, int *path) {
  int i = 0, j, weight, to, k = 0;
  GraphItem *item;
  GraphItem **items = (GraphItem**)calloc(this->edges, sizeof(GraphItem*));
  for(; i < this->capacity; i++) {
    path[i] = -1;
    if(this->items[i] == NULL) {
      continue;
    }
    for(j = 0; j < this->items[i]->length; j += 2) {
      to = this->items[i]->items[j];
      weight = this->items[i]->items[j+1];
      item = graph_item_create(weight, i, to);
      items[k++] = item;
    }
  }
  combsort((void**) items, k, -1, graph_item_compare);
  for(i = 0; i < k; i++) {
    item = items[i];
    if(item != NULL && path[item->to] == -1) {
      path[item->to] = item->from;
    }
    graph_item_destroy(&item);
  }
  free(items);
}
/*
* Function: graph_prim_min_spanning_tree
* ``````````````````````````````````````
* find minimum spanning tree using prim algorithm
* greedy
* take least costly edge leading to vertex
* close to dijkstra algorithm
* O(|E| log |V|) complexity
*
* this: graph
* from: start vertex
* path: map to store parents (construct a tree)
* distance: distance of each vertex from source
*
* return: nothing
*/
void graph_prim_min_spanning_tree(Graph *this, int from, int *path, int *distance) {
  int i = 0, j, to, weight;
  HeapArray *heap = heap_array_create(this->capacity + 1);
  GraphItem *item;
  for(; i < this->capacity; i++) {
    path[i] = -1;
    distance[i] = INT_MAX;
  }
  distance[from] = 0;
  heap_array_push_and_up(heap, graph_item_create(0, from, 0), -1, graph_item_compare);
  for(;heap->position;) {
    item = heap_array_pop_and_down(heap, -1, graph_item_compare);
    if(item == NULL) {
      continue;
    }
    for(j = 0; j < this->items[item->from]->length; j+=2) {
      to = this->items[item->from]->items[j];
      weight = this->items[item->from]->items[j+1];
      if(weight >= distance[to]) {
        continue;
      }
      distance[to] = weight;
      path[to] = item->from;
      heap_array_push_and_up(heap, graph_item_create(weight, to, 0), -1, graph_item_compare);
    }
    graph_item_destroy(&item);
  }
  heap_array_destroy(&heap);
}
/*
* Function: graph_random
* ``````````````````````
* generate random graph
* O(|V|`2) complexity
*
* this: graph
* prob: probability there is an edge
* insert: callback to insert edge
*
* return: nothing
*/
void graph_random(Graph *this, double prob, void (*insert)(Graph *this, int from, int to)) {
  double current;
  int i = 0, j;
  for(; i < this->capacity-1; i++) {
    for(j = i + 1; j < this->capacity; j++) {
      current = 1 / (double)(rand() % 100);
      if(current == 0) {
        current = 0.01;
      }
      if(current < prob) {
        continue;
      }
      insert(this, i, j);
    }
  }
}
void graph_random_congruential(Graph *this, DLONG seed, DLONG max, void (*insert)(Graph *this, int from, int to)) {
  int i = 2;
  DLONG from = random_number(seed) % max;
  DLONG to = random_number(from) % max;
  insert(this, from, to);
  for(; i < max; i += 2) {
    from = random_number(to) % max;
    to = random_number(from) % max;
    insert(this, from, to);
  }
}
void graph_sketch(Graph *this, int screen_width, int screen_height) {
  int i = 0;
  SDL_Event evt;
  SDL_Surface *screen;
  if(SDL_Init(SDL_INIT_VIDEO) != 0) {
    return;
  }
  screen = SDL_SetVideoMode(screen_width, screen_height, 24, SDL_SWSURFACE | SDL_DOUBLEBUF);
  if(screen == NULL) {
    return;
  }
  for(;1;) {
    for(;SDL_PollEvent(&evt);) {
      if(evt.type == SDL_QUIT || 
        (evt.type == SDL_KEYUP && evt.key.keysym.sym == SDLK_ESCAPE)) {
        SDL_FreeSurface(screen);
        SDL_Quit();
        return;
      }
    }
  }
  for(;i < this->capacity;i++) {
  }
}
void graph_destroy(Graph **this) {
  int i = 0;
  GraphList *graph_list;
  for(; i < (*this)->capacity; i++) {
    if((*this)->items[i] == NULL) {
      continue;
    }
    graph_list = (*this)->items[i];
    graph_list_destroy(&graph_list);
    (*this)->items[i] = NULL;
  }
  free((*this)->items);
  (*this)->items = NULL;
  free(*this);
  *this = NULL;
}
