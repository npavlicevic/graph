#include "graph_test.h"

int main() {
  int length = 7;
  Graph *graph = graph_create(length);
  int length_weighted = 8;
  Graph *graph_weighted = graph_create(length_weighted);
  int edges[] = {0,1,1,3,1,2,3,2,2,4,4,6,6,5};
  int i = 0;
  int edges_length = sizeof(edges) / sizeof(int);
  int edges_weighted[] = {0,1,4,0,6,5,1,2,5,2,7,2,7,3,4,7,5,7,7,6,3,3,4,8,4,5,6,5,6,2};
  int edges_weighted_length = sizeof(edges_weighted) / sizeof(int);
  for(; i < edges_length; i += 2) {
    graph_edge_insert(graph, edges[i], edges[i + 1]);
  }
  for(i = 0; i < edges_weighted_length; i += 3) {
    graph_edge_weighted_insert(graph_weighted, edges_weighted[i], edges_weighted[i + 1], edges_weighted[i + 2]);
  }
  random_number_test();
  graph_test(graph);
  graph_bfs_test(graph);
  graph_bfs_another_test(graph);
  graph_recursive_dfs_test(graph);
  graph_iterative_dfs_test(graph);
  graph_naive_dijkstra_test(graph_weighted);
  map_test();
  graph_map_dijkstra_test(graph_weighted);
  graph_heap_dijkstra_test(graph_weighted);
  graph_bellman_test(graph_weighted);
  graph_floyd_test(graph_weighted);
  graph_floyd_transitive_test(graph_weighted);
  graph_adjacency_matrix_test(graph);
  graph_adjacency_matrix_weighted_test(graph_weighted);
  graph_adjacency_matrix_weighted_boolean_test(graph_weighted);
  matrix_russians_boolean_test();
  matrix_multiply_boolean_test();
  matrix_multiply_russians_boolean_test();
  graph_boolean_transitive_test();
  graph_boolean_transitive_another_test();
  graph_kruskal_min_spanning_tree_test(graph_weighted);
  graph_prim_min_spanning_tree_test(graph_weighted);
  graph_destroy(&graph);
  graph_destroy(&graph_weighted);
  return 0;
}
