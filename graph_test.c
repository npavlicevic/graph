#include "graph_test.h"
void random_number_test() {
  int numbers[3];
  numbers[0] = random_number(1) % 1024;
  numbers[1] = random_number(numbers[0]) % 1024;
  numbers[2] = random_number(numbers[1]) % 1024;
  assert(numbers[0]);
  printf("%d\n", numbers[0]);
  assert(numbers[1]);
  printf("%d\n", numbers[1]);
  assert(numbers[2]);
  printf("%d\n\n", numbers[2]);
  assert(numbers[0] != numbers[1]);
  assert(numbers[1] != numbers[2]);
}
void graph_test(Graph *graph) {
  assert(graph_is_adjacent(graph, 1, 3));
  assert(graph_is_adjacent(graph, 1, 2));
  assert(graph_is_adjacent(graph, 2, 4));
}
void graph_bfs_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_bfs(graph, 0, path, distance);
  assert(path[3] == 1);
  assert(path[2] == 1);
  assert(path[4] == 2);
  assert(path[5] == 6);
  assert(distance[3] == 2);
  assert(distance[2] == 2);
  assert(distance[4] == 3);
  assert(distance[5] == 5);
  free(path);
  free(distance);
}
void graph_bfs_another_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_bfs(graph, 4, path, distance);
  assert(path[3] == 2);
  assert(path[2] == 4);
  assert(path[0] == 1);
  assert(path[6] == 4);
  assert(distance[2] == 1);
  assert(distance[1] == 2);
  assert(distance[3] == 2);
  assert(distance[5] == 2);
  free(path);
  free(distance);
}
void graph_recursive_dfs_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_recursive_dfs(graph, 0, path, distance);
  assert(path[3] == 1);
  assert(path[2] == 3);
  assert(path[4] == 2);
  assert(path[6] == 4);
  assert(distance[2] == 3);
  assert(distance[1] == 1);
  assert(distance[3] == 2);
  assert(distance[5] == 6);
  free(path);
  free(distance);
}
void graph_iterative_dfs_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_iterative_dfs(graph, 0, path, distance);
  assert(path[3] == 1);
  assert(path[2] == 1);
  assert(path[4] == 2);
  assert(path[6] == 4);
  assert(distance[2] == 2);
  assert(distance[1] == 1);
  assert(distance[3] == 2);
  assert(distance[5] == 5);
  free(path);
  free(distance);
}
void graph_naive_dijkstra_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_naive_dijkstra(graph, 0, path, distance);
  assert(distance[1] == 4);
  assert(distance[2] == 9);
  assert(distance[7] == 8);
  assert(distance[5] == 7);
  free(path);
  free(distance);
}
void map_test() {
  GraphList *map = graph_list_create(8);
  assert(!graph_list_map_is_set(map, 7));
  graph_list_map_set(map, 7);
  assert(graph_list_map_is_set(map, 7));
  graph_list_destroy(&map);
}
void graph_map_dijkstra_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_map_dijkstra(graph, 0, path, distance);
  assert(distance[1] == 4);
  assert(distance[2] == 9);
  assert(distance[7] == 8);
  assert(distance[5] == 7);
  free(path);
  free(distance);
}
void graph_heap_dijkstra_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_heap_dijkstra(graph, 0, path, distance);
  assert(distance[1] == 4);
  assert(distance[2] == 9);
  assert(distance[7] == 8);
  assert(distance[5] == 7);
  free(path);
  free(distance);
}
void graph_bellman_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_bellman(graph, 0, path, distance);
  assert(distance[1] == 4);
  assert(distance[2] == 9);
  assert(distance[7] == 8);
  assert(distance[5] == 7);
  free(path);
  free(distance);
}
void graph_floyd_test(Graph *graph) {
  Matrix *path = matrix_create(graph->capacity, graph->capacity);
  Matrix *distance = matrix_create(graph->capacity, graph->capacity);
  int *_path = calloc(graph->capacity, sizeof(int));
  graph_floyd(graph, path, distance);
  assert(distance->m[0][1] == 4);
  assert(distance->m[0][2] == 9);
  assert(distance->m[0][7] == 8);
  assert(distance->m[0][5] == 7);
  graph_floyd_path(_path, 0, 2, path);
  assert(_path[1] == 0);
  assert(_path[2] == 1);
  matrix_destroy(&path);
  matrix_destroy(&distance);
  free(_path);
}
void graph_floyd_transitive_test(Graph *graph) {
  Matrix *transitive = matrix_create(graph->capacity, graph->capacity);
  graph_floyd_transitive(graph, transitive);
  assert(transitive->m[0][5]);
  assert(transitive->m[0][4]);
  assert(transitive->m[0][3]);
  matrix_destroy(&transitive);
}
void graph_adjacency_matrix_test(Graph *graph) {
  Matrix *adj = matrix_create(graph->capacity, graph->capacity);
  graph_adjacency_matrix(graph, adj);
  assert(adj->m[1][0]);
  assert(adj->m[1][2]);
  assert(adj->m[1][3]);
  matrix_print(adj);
  matrix_destroy(&adj);
}
void graph_adjacency_matrix_weighted_test(Graph *this) {
  Matrix *adj = matrix_create(this->capacity, this->capacity);
  graph_adjacency_matrix_weighted(this, adj);
  assert(adj->m[1][0]);
  assert(adj->m[1][2]);
  assert(adj->m[2][1]);
  matrix_print(adj);
  matrix_destroy(&adj);
}
void graph_adjacency_matrix_weighted_boolean_test(Graph *this) {
  Matrix *adj = matrix_create(this->capacity, this->capacity);
  graph_adjacency_matrix_weighted_boolean(this, adj);
  assert(adj->m[1][0]);
  assert(adj->m[1][2]);
  assert(adj->m[2][1]);
  matrix_print(adj);
  matrix_destroy(&adj);
}
void matrix_russians_boolean_test() {
  Matrix *a = matrix_create(4, 4);
  matrix_russians_boolean(a);
  matrix_print(a);
  matrix_destroy(&a);
}
void matrix_multiply_boolean_test() {
  int a_content[] = {0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0};
  Matrix *a = matrix_create(4, 4);
  matrix_set_from_array(a, a_content);
  matrix_print(a);
  int b_content[] = {1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  Matrix *b = matrix_create(4, 4);
  matrix_set_from_array(b, b_content);
  matrix_print(b);
  Matrix *c = matrix_create(4, 4);
  matrix_multiply_boolean(c, a, b);
  matrix_print(c);
  assert(c->m[1][0]);
  assert(c->m[1][1]);
  matrix_destroy(&a);
  matrix_destroy(&b);
  matrix_destroy(&c);
}
void matrix_multiply_russians_boolean_test() {
  Matrix *russians = matrix_create(4, 4);
  matrix_russians_boolean(russians);
  matrix_print(russians);
  int a_content[] = {0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0};
  Matrix *a = matrix_create(4, 4);
  matrix_set_from_array(a, a_content);
  matrix_print(a);
  int b_content[] = {1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  Matrix *b = matrix_create(4, 4);
  matrix_set_from_array(b, b_content);
  matrix_print(b);
  Matrix *c = matrix_create(4, 4);
  matrix_multiply_russians_boolean(c, russians, a, b);
  matrix_print(c);
  matrix_destroy(&russians);
  matrix_destroy(&a);
  matrix_destroy(&b);
  matrix_destroy(&c);
}
void graph_boolean_transitive_test() {
  int content[] = {1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1};
  Matrix *a = matrix_create(4, 4);
  matrix_set_from_array(a, content);
  matrix_print(a);
  Matrix *b = matrix_create(4, 4);
  matrix_set_from_array(b, content);
  matrix_print(b);
  Matrix *c = matrix_create(4, 4);
  Matrix *russians = matrix_create(4, 4);
  matrix_russians_boolean(russians);
  matrix_print(russians);
  graph_boolean_transitive(c, russians, a, b, 1);
  matrix_print(a);
  matrix_print(b);
  assert(c->m[0][0]);
  assert(c->m[0][1]);
  assert(c->m[0][2]);
  assert(!c->m[0][3]);
  matrix_print(c);
  matrix_destroy(&a);
  matrix_destroy(&b);
  matrix_destroy(&c);
  matrix_destroy(&russians);
}
void graph_boolean_transitive_another_test() {
  int content[] = {1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1};
  Matrix *a = matrix_create(4, 4);
  matrix_set_from_array(a, content);
  matrix_print(a);
  Matrix *b = matrix_create(4, 4);
  matrix_set_from_array(b, content);
  matrix_print(b);
  Matrix *c = matrix_create(4, 4);
  Matrix *russians = matrix_create(4, 4);
  matrix_russians_boolean(russians);
  matrix_print(russians);
  graph_boolean_transitive(c, russians, a, b, 1);
  matrix_print(a);
  matrix_print(b);
  assert(c->m[0][0]);
  assert(c->m[0][1]);
  assert(!c->m[0][2]);
  assert(!c->m[0][3]);
  matrix_print(c);
  matrix_destroy(&a);
  matrix_destroy(&b);
  matrix_destroy(&c);
  matrix_destroy(&russians);
}
void graph_kruskal_min_spanning_tree_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  graph_kruskal_min_spanning_tree(graph, path);
  assert(path[1] == 0);
  assert(path[2] == 7);
  assert(path[3] == 7);
  free(path);
}
void graph_prim_min_spanning_tree_test(Graph *graph) {
  int *path = calloc(graph->capacity, sizeof(int));
  int *distance = calloc(graph->capacity, sizeof(int));
  graph_prim_min_spanning_tree(graph, 0, path, distance);
  assert(path[1] == 0);
  assert(path[2] == 7);
  assert(path[3] == 7);
  free(path);
  free(distance);
}
