#ifndef GRAPH_H
#define GRAPH_H
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include "heap_array.h"
#include "sort.h"
DLONG random_number(DLONG seed);
typedef struct matrix {
  int rows;
  int cols;
  int **m;
} Matrix;
Matrix *matrix_create(int rows, int cols);
void matrix_russians_boolean(Matrix *this);
void matrix_multiply_boolean(Matrix *this, Matrix *a, Matrix *b);
void matrix_multiply_russians_boolean(Matrix *this, Matrix *russians, Matrix *a, Matrix *b);
int matrix_row_boolean(Matrix *this, int row, int column, int step);
int matrix_column_boolean(Matrix *this, int row, int column, int step);
int scalar_multiply_boolean(int one, int two);
void matrix_set(Matrix *this, int col, int row, int value);
void matrix_set_from_array(Matrix *this, int *array);
void matrix_copy(Matrix *this, Matrix *src);
void matrix_print(Matrix *this);
void matrix_destroy(Matrix **this);
typedef struct q {
  int first;
  int last;
  int capacity;
  int *items;
} Q;
Q *q_create(int capacity);
void q_push(Q *this, int item);
int q_fetch(Q *this);
void q_pop(Q *this);
void q_destroy(Q **this);
typedef struct graph_item {
  int key;
  int from;
  int to;
} GraphItem;
GraphItem *graph_item_create(int key, int from, int to);
void graph_item_destroy(GraphItem **this);
DLONG graph_item_compare(void *other, void *key);
typedef struct graph_list {
  int length;
  int capacity;
  int *items;
} GraphList;
GraphList *graph_list_create(int capacity);
void graph_list_append(GraphList *this, int item);
int graph_list_get(GraphList *this);
void graph_list_pop(GraphList *this);
void graph_list_map_set(GraphList *this, int item);
void graph_list_map_unset(GraphList *this, int item);
int graph_list_map_is_set(GraphList *this, int item);
void graph_list_destroy(GraphList **this);
typedef struct graph {
  int capacity;
  int edges;
  GraphList **items;
} Graph;
Graph *graph_create(int capacity);
void graph_edge_insert(Graph *this, int from, int to);
void graph_edge_directed_insert(Graph *this, int from, int to);
void graph_edge_weighted_insert(Graph *this, int from, int to, int weight);
void graph_edge_weighted_directed_insert(Graph *this, int from, int to, int weight);
int graph_is_adjacent(Graph *this, int from, int to);
void graph_bfs(Graph *this, int from, int *path, int *distance);
void graph_recursive_dfs(Graph *this, int from, int *path, int *distance);
void graph_iterative_dfs(Graph *this, int from, int *path, int *distance);
void graph_naive_dijkstra(Graph *this, int from, int *path, int *distance);
void graph_map_dijkstra(Graph *this, int from, int *path, int *distance);
void graph_heap_dijkstra(Graph *this, int from, int *path, int *distance);
void graph_bellman(Graph *this, int from, int *path, int *distance);
void graph_floyd(Graph *this, Matrix *path, Matrix *distance);
void graph_floyd_path(int *this, int from, int to, Matrix *path);
void graph_floyd_transitive(Graph *this, Matrix *transitive);
void graph_adjacency_matrix(Graph *this, Matrix *adj);
void graph_adjacency_matrix_weighted(Graph *this, Matrix *adj);
void graph_adjacency_matrix_weighted_boolean(Graph *this, Matrix *adj);
void graph_boolean_transitive(Matrix *this, Matrix *russians, Matrix *a, Matrix *b, int degree);
void graph_kruskal_min_spanning_tree(Graph *this, int *path);
void graph_prim_min_spanning_tree(Graph *this, int from, int *path, int *distance);
void graph_random(Graph *this, double prob, void (*insert)(Graph *this, int from, int to));
void graph_random_congruential(Graph *this, DLONG seed, DLONG max, void (*insert)(Graph *this, int from, int to));
void graph_sketch(Graph *this, int screen_width, int screen_height);
void graph_destroy(Graph **this);
// TODO some ints to voids
// TODO can use heap array or a hash for graph
// TODO q lib
// TODO graph list to lib as array list
// TODO matrix to lib
// TODO matrix general type? as other structs
// TODO matrix primitive types as compile targets
// TODO scalar_binary_multiply to lib
// TODO Q in terms of list
// TODO algorithms on matrices too
#endif
