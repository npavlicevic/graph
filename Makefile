#
# Makefile 
# some algorithms on graphs
#

CC=gcc
AR=ar
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic
OBJ_CFLAGS=-c -static
AR_FLAGS=rcs
LIBS=-lm
LIBS_ARRAY=-larrayc
LIBS_HEAP_ARRAY=-lheaparrayc
LIBS_SORT=-lsortc
LIBS_SDL=-lSDL

FILES=graph.c graph_test.c main.c
CLEAN=main

all: main

main: ${FILES}
	${CC} ${CFLAGS} $^ -o main ${LIBS} ${LIBS_HEAP_ARRAY} ${LIBS_ARRAY} ${LIBS_SORT} ${LIBS_SDL}

clean: ${CLEAN}
	rm $^
