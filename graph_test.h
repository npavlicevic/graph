#ifndef GRAPH_TEST_H
#define GRAPH_TEST_H
#include <assert.h>
#include "graph.h"
void random_number_test();
void graph_test(Graph *graph);
void graph_bfs_test(Graph *graph);
void graph_bfs_another_test(Graph *graph);
void graph_recursive_dfs_test(Graph *graph);
void graph_iterative_dfs_test(Graph *graph);
void graph_naive_dijkstra_test(Graph *graph);
void map_test();
void graph_map_dijkstra_test(Graph *graph);
void graph_heap_dijkstra_test(Graph *graph);
void graph_bellman_test(Graph *graph);
void graph_floyd_test(Graph *graph);
void graph_floyd_transitive_test(Graph *graph);
void graph_adjacency_matrix_test(Graph *graph);
void graph_adjacency_matrix_weighted_test(Graph *graph);
void graph_adjacency_matrix_weighted_boolean_test(Graph *graph);
void matrix_russians_boolean_test();
void matrix_multiply_boolean_test();
void matrix_multiply_russians_boolean_test();
void graph_boolean_transitive_test();
void graph_boolean_transitive_another_test();
void graph_kruskal_min_spanning_tree_test(Graph *graph);
void graph_prim_min_spanning_tree_test(Graph *graph);
#endif
